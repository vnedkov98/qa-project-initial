using RestSharp;
using System.Net;
using System.Text.Json;

namespace RestSharpAPITests
{
    public class RestSharpAPI_Tests
    {
        private RestClient client;
        private const string baseUrl = "https://valentintaskboardjs.valentin9831.repl.co/api/";

        [SetUp]
        public void SetUp()
        {
            this.client = new RestClient(baseUrl);
        }


        [Test]
        public void List_all_Tasks()
        {
            var request = new RestRequest("/tasks/board/done", Method.Get);
            var response = this.client.Execute(request);

            var tasks = JsonSerializer.Deserialize<List<Tasks>>(response.Content);
            Assert.That(tasks[0].title, Is.EqualTo("Project skeleton"));
        }
        [Test]
        public void First_Result_HomePage()
        {
            var request = new RestRequest("/tasks/search/home", Method.Get);
            var response = this.client.Execute(request);

            var tasks = JsonSerializer.Deserialize<List<Tasks>>(response.Content);
            Assert.That(tasks[0].title, Is.EqualTo("Home page"));
        }
        [Test]
        public void First_Result_Randum()
        {
            var request = new RestRequest("/tasks/search/missing{randnum}", Method.Get);
            var response = this.client.Execute(request);

            var tasks = JsonSerializer.Deserialize<List<Tasks>>(response.Content);
            Assert.That(response.Content, Is.EqualTo("[]"));
        }
        [Test]
        public void Create_New_Task_Invalid_data()
        {
            var request = new RestRequest("/tasks", Method.Post);

            var reqBody = new
            {
                description = "Desc from Rest#",
                board = "Open"
            };

            request.AddBody(reqBody);
            var response = this.client.Execute(request);

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Content, Is.EqualTo("{\"errMsg\":\"Title cannot be empty!\"}"));

        }
        [Test]
        public void Create_New_Task_Valid_data()
        {
            var request = new RestRequest("tasks", Method.Post);

            var reqBody = new
            {
                title = "Sometitle",
                description = "Desc from Rest#",
                board = "Open"
            };

            request.AddBody(reqBody);
            var response = this.client.Execute(request);

            var taskObject = JsonSerializer.Deserialize<taskObject>(response.Content);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));

            Assert.That(taskObject.msg, Is.EqualTo("Task added."));
            Assert.That(taskObject.task.id,Is.GreaterThan(0));
            Assert.That(taskObject.task.title, Is.EqualTo(reqBody.title));
            Assert.That(taskObject.task.description, Is.EqualTo(reqBody.description));
            Assert.That(taskObject.task.board.id, Is.GreaterThan(0));
            Assert.That(taskObject.task.board.name, Is.EqualTo("Open"));
            Assert.That(taskObject.task.dateCreated, Is.Not.Empty);
            Assert.That(taskObject.task.dateModified, Is.Not.Empty);




        }
    }
    }      