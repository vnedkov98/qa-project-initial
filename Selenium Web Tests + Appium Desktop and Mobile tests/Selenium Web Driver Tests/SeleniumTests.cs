using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace SeleniumTests
{
    public class SeleniumTests
    {

        public WebDriver driver;
        private const string baseUrl = "https://shorturl.valentin9831.repl.co/";
        [SetUp]
        public void OpenPage()
        {

            driver = new ChromeDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
        }
        [TearDown]
        public void ClosePage()
        {
            driver.Quit();
        }
        [Test]
        public void OpenShortUrlPageAndAssertThatTableHoldsOriginalUrlLabel()
        {
            driver.Url = baseUrl;
            var ShortUrlLink = driver.FindElement(By.XPath("//a[@href='/urls'][contains(.,'Short URLs')]"));
            ShortUrlLink.Click();

            var OriginalLinkBox = driver.FindElement(By.XPath("/html/body/main/table/thead/tr/th[1]"));

            Assert.That(OriginalLinkBox.Text, Is.EqualTo("Original URL"));
        }
        [Test]
        public void CreateShortUrlAndAssertIt()
        {
            var UrlToAdd = "https://www.youtube.com/watch?v=-LwBbLa_Vhc";
            driver.Url = baseUrl;
            var AddShortUrlLink = driver.FindElement(By.XPath("/html/body/header/a[3]"));
            AddShortUrlLink.Click();

            var inputUrlField = driver.FindElement(By.Id("url"));
            inputUrlField.SendKeys(UrlToAdd);

            var shortCodeField = driver.FindElement(By.Id("code"));
            shortCodeField.SendKeys("Bocchi");

            var createBtn = driver.FindElement(By.XPath("//button[@type='submit'][contains(.,'Create')]"));
            createBtn.Click();

            Assert.That(driver.PageSource.Contains(UrlToAdd));


        }
        [Test]
        public void CreateInvalidShorUrlAndAssertItIsInvalid()
        {
            driver.Url = baseUrl;
            var AddShortUrlLink = driver.FindElement(By.XPath("/html/body/header/a[3]"));
            AddShortUrlLink.Click();

            var inputUrlField = driver.FindElement(By.Id("url"));
            inputUrlField.SendKeys("https://www.youtube.com/watch?v=-LwBbLa_Vhc");

            var shortCodeField = driver.FindElement(By.Id("code"));
            shortCodeField.SendKeys("Bocchi!");

            var createBtn = driver.FindElement(By.XPath("//button[@type='submit'][contains(.,'Create')]"));
            createBtn.Click();

            var errorMsg = driver.FindElement(By.XPath("/html/body/div"));
            Assert.That(errorMsg.Text, Is.EqualTo("Short code holds invalid chars!"));
        }

        [Test]
        public void VisitNonExistingUrlAndAssertThatErrorIsReturned() 
        {
            driver.Url = "http://shorturl.nakov.repl.co/go/invalid536524";
            var errorMsg = driver.FindElement(By.XPath("//div[@class='err']"));
            Assert.That(errorMsg.Text, Is.EqualTo("Cannot navigate to given short URL"));
        }
        [Test]
        public void VisitValidUrlAndAssertItCountIncreases()
        {
            driver.Url = baseUrl;
            var linkShortUrl = driver.FindElement(By.LinkText("Short URLs"));
            linkShortUrl.Click();

            var tableFirstRow = driver.FindElements(By.CssSelector("table > tbody > tr")).First();
            var oldCounter = int.Parse(tableFirstRow.FindElements(By.CssSelector("td")).Last().Text);

            var linkToClickCell = tableFirstRow.FindElements(By.CssSelector("td"))[1];

            var linkToClick = linkToClickCell.FindElement(By.TagName("a"));
            linkToClick.Click();

            driver.SwitchTo().Window(driver.WindowHandles[0]);

            driver.Navigate().Refresh();

            tableFirstRow = driver.FindElements(By.CssSelector("table > tbody > tr")).First();
            var newCounter = int.Parse(tableFirstRow.FindElements(By.CssSelector("td")).Last().Text);

            Assert.That(newCounter, Is.EqualTo(oldCounter + 1));

        }
    }
}