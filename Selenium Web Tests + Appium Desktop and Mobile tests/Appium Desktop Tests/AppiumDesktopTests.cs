using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;

namespace AppiumDesktopTests
{
    public class AppiumDesktopTests
    {


        private WindowsDriver<WindowsElement> driver;
        private AppiumOptions options;

        private const string AppiumServerIP = "http://[::1]:4723/wd/hub";
        private const string AppPath = @"C:\\Users\\Valentin\\Desktop\\Automation FE exam prep 1\\ShortURL-DesktopClient-v1.0.net6\\ShortURL-DesktopClient.exe";
        private const string AppUrl = "https://shorturl.valentin9831.repl.co/api";



        [SetUp]
        public void AppStartup () 
        { 
        options = new AppiumOptions ();
            options.AddAdditionalCapability("app", AppPath);
            driver = new WindowsDriver<WindowsElement>(new Uri(AppiumServerIP), options);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2000);
        }

        [TearDown]
        public void AppCLose() 
        {
            driver.Quit();
        }

        [Test]
        public void CreateNewShortUrlAndAssert() 
        
        {
            var UrlTextBox = driver.FindElementByAccessibilityId("textBoxApiUrl");
            UrlTextBox.Clear();
            UrlTextBox.SendKeys(AppUrl);

            var SubmitBtn = driver.FindElementByAccessibilityId("buttonConnect");
            SubmitBtn.Click();

            var AddButton = driver.FindElementByAccessibilityId("buttonAdd");
            AddButton.Click();

            var UrlToAddField = driver.FindElementByAccessibilityId("textBoxURL");
            UrlToAddField.SendKeys("https://www.youtube.com/watch?v=iKBCVZqqooY");

            var CreateBtn = driver.FindElementByAccessibilityId("buttonCreate");
            CreateBtn.Click();

            var result = driver.FindElementByName("https://www.youtube.com/watch?v=iKBCVZqqooY");
            Assert.IsNotEmpty(result.Text);
            Assert.That(result.Text, Is.EqualTo("https://www.youtube.com/watch?v=iKBCVZqqooY"));
        }
    } 
}