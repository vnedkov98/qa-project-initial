using NUnit.Framework;

namespace Summator.Unit_Tests
{
    public class SummatorTests
    {
        [Test]
        public void Test_Summator_SumTwoPositiveNumbers()
        {
            var nums = new int[] { 1, 2 };
            var actual = Summator.Sum(nums);

            var expected = 3;
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Test_Summator_SumTwoNegativeNumbers()
        {
            var nums = new int[] { -99, -2 };
            var actual = Summator.Sum(nums);

            var expected = -101;
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Test_Summator_SumOnePositiveNumber()
        {
            var nums = new int[] { 1000000000 };
            var actual = Summator.Sum(nums);

            var expected = 1000000000;
            Assert.AreEqual(actual, expected);
        }

        [Test]
        public void Test_Summator_ZeroNumbers()
        {
            var nums = new int[] { };
            var actual = Summator.Sum(nums);

            var expected = 0;
            Assert.AreEqual(actual, expected);
        }
        [Test]
        public void Test_Summator_BigNumbers()
        {
            var nums = new int[] { 2000000000, 2000000000, 2000000000 };
            var actual = Summator.Sum(nums);

            var expected = 6000000000;
            Assert.AreEqual(actual, expected);
        }
        [Test]
        public void Test_Summator_AverageOfThreeNumbers()
        {
            var nums = new int[] { 3, 6, 9 };
            var actual = Summator.Average(nums);

            var expected = 6;
            Assert.AreEqual(actual, expected);
        }
        [Test]
        public void Test_Summator_Multiplicity()
        {
            var nums = new int[] { 6,6 };
            var actual = Summator.Miltiply(nums);
            var expected = 24;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }
}