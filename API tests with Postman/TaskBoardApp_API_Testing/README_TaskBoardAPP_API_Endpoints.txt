Project Link https://replit.com/@SoftUniQA/TaskBoardJS

API endpoints:

TaskBoard exposes a RESTful API, available at:
https://taskboard.softuniqa.repl.co/api
The following endpoints are supported:
GET /api – list all API endpoints
GET /api/tasks – list all tasks (returns JSON array of tasks)
GET /api/tasks/id – returns a task by given id
GET /api/tasks/search/keyword – list all tasks matching given keyword
GET /api/tasks/board/boardName – list tasks by board name
POST /api/tasks – create a new task (post a JSON object in the request body, e.g. {"title":"Add Tests", "description":"API + UI tests", "board":"Open"})
PATCH /api/tasks/id – edit task by id (send a JSON object in the request body, holding the fields to modify, e.g. {"title":"changed title", "board":"Done"})
DELETE /api/tasks/id – delete task by id
GET /api/boards – list all boards